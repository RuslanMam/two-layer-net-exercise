import numpy as np

class TwoLayerNet(object):
    """
    A two-layer fully-connected neural network. The net has an input dimension
    of N, a hidden layer dimension of H, and performs classification over C
    classes. We train the network with a softmax loss function and L2
    regularization on the weight matrices. The network uses a ReLU nonlinearity
    after the first fully connected layer.

    In other words, the network has the following architecture:

    input - fully connected layer - ReLU - fully connected layer - softmax

    The outputs of the second fully-connected layer are the scores for each
    class.
    """

    def __init__(self, input_size, hidden_size, output_size, std=1e-4):
        """
        Initialize the model. Weights are initialized to small random values and
        biases are initialized to zero. Weights and biases are stored in the
        variable self.params, which is a dictionary with the following keys:

        W1: First layer weights; has shape (D, H)
        b1: First layer biases; has shape (H,)
        W2: Second layer weights; has shape (H, C)
        b2: Second layer biases; has shape (C,)

        Inputs:
        - input_size: The dimension D of the input data.
        - hidden_size: The number of neurons H in the hidden layer.
        - output_size: The number of classes C.
        """
        self.params = {}
        self.params['W1'] = std * np.random.randn(input_size, hidden_size)
        self.params['b1'] = np.zeros(hidden_size)
        self.params['W2'] = std * np.random.randn(hidden_size, output_size)
        self.params['b2'] = np.zeros(output_size)

    def loss(self, X, y=None, reg=0.0):
        """
        Compute the loss and gradients for a two layer fully connected neural
        network.

        Inputs:
        - X: Input data of shape (N, D). Each X[i] is a training sample.
        - y: Vector of training labels. y[i] is the label for X[i], and each
          y[i] is an integer in the range 0 <= y[i] < C. This parameter is
          optional; if it is not passed then we only return scores, and if it is
          passed then we instead return the loss and gradients.
        - reg: Regularization strength.

        Returns:
        If y is None, return a matrix scores of shape (N, C) where scores[i, c]
        is the score for class c on input X[i].

        If y is not None, instead return a tuple of:
        - loss: Loss (data loss and regularization loss) for this batch of
          training samples.
        - grads: Dictionary mapping parameter names to gradients of those
          parameters  with respect to the loss function; has the same keys as
          self.params.
        """
        # Unpack variables from the params dictionary
        W1, b1 = self.params['W1'], self.params['b1']
        W2, b2 = self.params['W2'], self.params['b2']
        N, _ = X.shape

        # Compute the forward pass
        ########################################################################
        # TODO: Perform the forward pass, computing the class scores for the   #
        # input. Store the result in the scores variable, which should be an   #
        # array of shape (N, C).                                               #         
        ########################################################################
        scores = np.hstack([X, np.ones((X.shape[0], 1))])
        X_with_ones = np.copy(scores)

        my_W1= np.vstack([W1, b1])
        
        scores = np.dot(scores, my_W1)        
        scores[scores < 0] = 0
       
        derivatives = np.copy(scores)
        derivatives[derivatives>0] = 1
        
        scores = np.hstack([scores, np.ones((scores.shape[0], 1))])
        scores_before_entering_softmax = np.copy(scores)

        my_W2 = np.vstack([W2, b2])
        scores = np.dot(scores, my_W2)
                
        ########################################################################
        #                              END OF YOUR CODE                        #
        ########################################################################

        # If the targets are not given then jump out, we're done
        if y is None:
            return scores

        # Compute the loss
        loss = 0
        ########################################################################
        # TODO: Finish the forward pass, and compute the loss. This should     #
        # include both the data loss and L2 regularization for W1 and W2. Store#
        # the result in the variable loss, which should be a scalar. Use the   #
        # Softmax classifier loss. So that your results match ours, multiply   #
        # the regularization loss by 0.5                                       #
        ########################################################################
        matrix = np.exp(scores)  
        summs = np.sum(matrix, axis = 1, keepdims= 1)
        matrix_divided = matrix/summs

        loss -= np.sum(np.log(matrix_divided[range(N), y]))
        loss /= N
        loss += 0.5 * reg * (np.sum(my_W1 * my_W1) + np.sum(my_W2 * my_W2)) 
  
        ones = np.zeros_like(matrix_divided)
        ones[range(N), y] = 1
                
        ########################################################################
        #                              END OF YOUR CODE                        #
        ########################################################################
        # Backward pass: compute gradients
        grads = {}
        ########################################################################
        # TODO: Compute the backward pass, computing the derivatives of the    #
        # weights and biases. Store the results in the grads dictionary. For   #
        # example, grads['W1'] should store the gradient on W1, and be a matrix#
        # of same size                                                         #
        ########################################################################
        dLoss = matrix_divided - ones
        dW = scores_before_entering_softmax.transpose().dot(dLoss)
        dW /= len(scores)
        dW += reg * my_W2

        grads["W2"], grads["b2"] = np.split(dW, [-1])

        my_W2 = np.split(my_W2, [-1])[0]

        dW1 = np.dot(dLoss, my_W2.T)
        dW1 = dW1 * derivatives
        dW1 = np.dot(X_with_ones.T, dW1)
        dW1 /= N
        dW1 += reg * my_W1

        grads["W1"], grads["b1"] = np.split(dW1, [-1])
        ########################################################################
        #                              END OF YOUR CODE                        #
        ########################################################################

        return loss, grads

    def train(self, X, y, X_val, y_val,
              learning_rate=1e-3, learning_rate_decay=0.95,
              reg=1e-5, num_iters=100,
              batch_size=200, verbose=False):
        """
        Train this neural network using stochastic gradient descent.

        Inputs:
        - X: A numpy array of shape (N, D) giving training data.
        - y: A numpy array f shape (N,) giving training labels; y[i] = c means
          that X[i] has label c, where 0 <= c < C.
        - X_val: A numpy array of shape (N_val, D) giving validation data.
        - y_val: A numpy array of shape (N_val,) giving validation labels.
        - learning_rate: Scalar giving learning rate for optimization.
        - learning_rate_decay: Scalar giving factor used to decay the learning
          rate after each epoch.
        - reg: Scalar giving regularization strength.
        - num_iters: Number of steps to take when optimizing.
        - batch_size: Number of training examples to use per step.
        - verbose: boolean; if true print progress during optimization.
        """
        # pylint: disable=too-many-arguments, too-many-locals
        num_train = X.shape[0]
        iterations_per_epoch = max(num_train // batch_size, 1)

        # Use SGD to optimize the parameters in self.model
        a1, b1 = self.params['W1'].shape
        a2, b2 = self.params['W2'].shape
        dW1 = np.empty([a1, b1])
        dW2 = np.empty([a2, b2])
        db1 = np.empty(b1)
        db2 = np.empty(b2)

        for it in range(num_iters):
            ####################################################################
            # TODO: Create a random minibatch of training data and labels,     #
            # storing hem in X_batch and y_batch respectively.                 #
            ####################################################################
            random_numbers = np.random.choice(num_train, batch_size)
            X_batch = X[random_numbers]
            y_batch = y[random_numbers]
            ####################################################################
            #                             END OF YOUR CODE                     #
            ####################################################################
            ## Compute loss and gradients using the current minibatch
            loss, grads = self.loss(X=X_batch, y=y_batch, reg=reg)

            ####################################################################
            # TODO: Use the gradients in the grads dictionary to update the    #
            # parameters of the network (stored in the dictionary self.params) #
            # using stochastic gradient descent. You'll need to use the        #
            # gradients stored in the grads dictionary defined above.          #
            ####################################################################

            db1[grads["b1"][0]<0] = learning_rate
            db1[grads["b1"][0]>0] = -learning_rate
            self.params["b1"] += db1
            
            db2[grads["b2"][0]<0] = learning_rate
            db2[grads["b2"][0]>0] = -learning_rate
            self.params["b2"] += db2

            dW1[grads["W1"]<0] = learning_rate
            dW1[grads["W1"]>0] = -learning_rate
            self.params["W1"] += dW1
            
            dW2[grads["W2"]<0] = learning_rate
            dW2[grads["W2"]>0] = -learning_rate
            self.params["W2"] += dW2
    
            ####################################################################
            #                             END OF YOUR CODE                     #
            ####################################################################

            if verbose and it % 100 == 0:
                print('iteration %d / %d: loss %f' % (it, num_iters, loss))

            # Every epoch, check train and val accuracy and decay learning rate.
            if it % iterations_per_epoch == 0:

                # Decay learning rate
                learning_rate *= learning_rate_decay

    def predict(self, X):
        """
        Use the trained weights of this two-layer network to predict labels for
        data points. For each data point we predict scores for each of the C
        classes, and assign each data point to the class with the highest score.

        Inputs:
        - X: A numpy array of shape (N, D) giving N D-dimensional data points to
          classify.

        Returns:
        - y_pred: A numpy array of shape (N,) giving predicted labels for each
          of the elements of X. For all i, y_pred[i] = c means that X[i] is
          predicted to have class c, where 0 <= c < C.
        """

        ########################################################################
        # TODO: Implement this function; it should be VERY simple!             #
        ########################################################################
        scores = self.loss(X)
        matrix = np.exp(scores)  
        summs = np.sum(matrix, axis = 1, keepdims = 1)
        matrix_divided = matrix/summs
        y_pred = np.argmax(matrix_divided , axis = 1) 
        ########################################################################
        #                              END OF YOUR CODE                        #
        ########################################################################

        return y_pred


def neuralnetwork_hyperparameter_tuning(X_train, y_train, X_val, y_val):

    ############################################################################
    # TODO: Tune hyperparameters using the validation set. Store your best     #
    # trained model in best_net.                                               #
    #                                                                          #
    # To help debug your network, it may help to use visualizations similar to #
    # the  ones we used above; these visualizations will have significant      #
    # qualitative differences from the ones we saw above for the poorly tuned  #
    # network.                                                                 #
    #                                                                          #
    # Tweaking hyperparameters by hand can be fun, but you might find it useful#
    # to  write code to sweep through possible combinations of hyperparameters #
    # automatically like we did on the previous exercises.                     #
    ############################################################################
    input_size = 32 * 32 * 3
    hidden_size = 50
    num_classes = 10
    
    best_net = TwoLayerNet(input_size, hidden_size, num_classes)
    best_net.train(X_train, y_train, X_val, y_val,
            num_iters=5000, batch_size=1000,
            learning_rate=0.001, learning_rate_decay=0.95,
            reg=0, verbose=True)
    
    # Actually I should use code below instead of code above, but it takes to much time.  #
    # That is why I just use the best parameters that I have found using the code below.  #

    '''
    net = TwoLayerNet(input_size, hidden_size, num_classes)
    best_val=0
    
    for our_batch_size in [500, 1000]:                              
         for our_learning_rate in [0.0001, 0.001]:  
                for our_reg in [0.0, 0.5, 1, 1.5]:
                    net = TwoLayerNet(input_size, hidden_size, num_classes)
                    net.train(X_train, y_train, X_val, y_val, our_learning_rate, 0.95, our_reg, 3000, our_batch_size, verbose=True)
                    val_acc = (net.predict(X_val) == y_val).mean()
                    if val_acc > best_val:
                        best_net= net
                        best_inputs = [our_batch_size, our_learning_rate, our_reg]
    print("The best batch size, learning rate and regularization are: ", best_inputs)
    '''

   # Predict on the validation set
    val_acc = (best_net.predict(X_val) == y_val).mean()
    print('Validation accuracy: ', val_acc)
    ############################################################################
    #                               END OF YOUR CODE                           #
    ############################################################################
    return best_net
