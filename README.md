
**Important**: Not all code is mine. It is a homework, the source for the tasks is here: https://git.lamf.de/Uni/introduction-2-deep-learning/-/tree/master/assignments, and the original source for the tasks here: https://cs231n.github.io/assignments2020/assignment1 (Stanford Vision Lab). Both sources are public, and publication of solutions was not forbidden. The code that was written by me can be recognized using comments by the keyword TODO. The file two_layer_net.py is the main file that was written by me. By concerns, e.g. if somebody copies the answers, please write to my email address ruslanmammadov48@gmail.com (please, take into consideration that this Stanford course is very popular, and I am not the only one who published solutions).
 
This program is an exercise that I have got in the Deep Learning course. In this exercise I should create a two layer model "input - fully connected layer - ReLu - fully connected layer - softmax". This model learns how to recognize cats, birds, jets and so on using pictures from CIFAR10. 

Warning: My part was written 2018, it was my first experience with machine learning.
 
To execute the program, write in terminal:
python3 two_layer_net.py
or:
py two_layer_net.py
 
If you do not have libraries, load them using commands:
python3 -m pip install numpy
python3 -m pip install matplotlib
or:
py -m pip install numpy
py -m pip install matplotlib
 
If python3 and py do not work, then you should install python.