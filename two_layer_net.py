#-*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pickle as pickle
import os, random

from exercise_code.data_utils import load_CIFAR10, get_CIFAR10_data

# load_CIFAR10 and get_CIFAR10_data were not written by me.
# TwoLayerNet, neuralnetwork_hyperparameter-tuning and two_layer_net.py were written by me.
from exercise_code.classifiers.neural_net import TwoLayerNet, neuralnetwork_hyperparameter_tuning

# Load data.
X_test_raw, y_test_raw, X_train, y_train, X_val, y_val, X_test, y_test, X_dev, y_dev= get_CIFAR10_data()
print('The pictures from CIFAR10 were loaded.')


# Look for the best parameters and train the model.
print('TwoLayerNet will be training using this pictures.')
print('Actually TwoLayerNet should be trained with different parameters, but it takes to much time.')
print('That is why I just use the best parameters, that I have found.')
best_net = neuralnetwork_hyperparameter_tuning(X_train, y_train, X_val, y_val)


# Test accuracy.
test_acc = (best_net.predict(X_test) == y_test).mean()
print('Test accuracy: ', test_acc)

# Visualize.
classes = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
sample = np.empty([1, len(X_test[0])])
for i in range(1, 11):
    index = random.randint(0, len(X_test_raw)-1)
    plt.subplot(2, 5, i)
    plt.imshow(X_test_raw[index].astype('uint8'))
    plt.axis('off')
    sample[0] = X_test[index]
    plt.title(classes[best_net.predict(sample)[0]])
plt.show()

# Save the model.
model = {'TwoLayerNet': best_net}
if not os.path.exists('models'):
    os.makedirs('models')
pickle.dump(model, open('models/TwoLayerNet.p', 'wb'))


